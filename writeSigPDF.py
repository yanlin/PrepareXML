import numpy as np
import sys
import yaml

Mean = {}
Sigma = {}
AlphaLo = {}
AlphaHi = {}

def writeSigXML():

    readSigPar()

    for i in range(15):
        cate = "cate{0}".format(i+1)
        filename = "signal_ttHCP_{0}.xml".format(cate)
        f = open(filename, "w")

        f.write("<!DOCTYPE Model SYSTEM 'AnaWSBuilder.dtd'>\n")
	f.write("<Model Type=\"UserDef\" CacheBinning=\"100000\">\n")
        f.write("<Item Name=\"muCBNom[{0}]\"/>\n".format(Mean[cate]))
	f.write("<Item Name=\"sigmaCBNom[{0}]\"/>\n".format(Sigma[cate]))
	f.write("<Item Name=\"alphaCBLo[{0}]\"/>\n".format(AlphaLo[cate]))
	f.write("<Item Name=\"alphaCBHi[{0}]\"/>\n".format(AlphaHi[cate]))
	f.write("<Item Name=\"nCBLo[1.0]\"/>\n")
	f.write("<Item Name=\"nCBHi[1.0]\"/>\n")
        f.write("<Item Name=\"expr::muCB('(@0+@1-125)',muCBNom,mH[125.09])\"/>\n")
        f.write("<Item Name=\"prod::sigmaCB(sigmaCBNom)\"/>\n")
	f.write("<ModelItem Name=\"RooTwoSidedCBShape::signal(:observable:, muCB, sigmaCB, alphaCBLo, nCBLo, alphaCBHi, nCBHi)\"/>\n")
	f.write("</Model>\n")

def readSigPar():
   
    siglist = [] 
    fin = open("sigPara.txt", "r")
    for fline in fin.readlines():
        fline = fline.rstrip("\n")
        siglist.append(fline)

    index = 0
    for i in range(len(siglist)):
        if "Had" in siglist[i] or "Lep" in siglist[i]:
           index = index + 1
           print siglist[i]
           cate = "cate{0}".format(str(index))
           Mean[cate] = siglist[i+1]  
           Sigma[cate] = siglist[i+2]
           AlphaLo[cate] = siglist[i+3]
           AlphaHi[cate] = siglist[i+4]
    print Mean
    print Sigma
    print AlphaLo
    print AlphaHi

def main():
    #readSigPar()
    writeSigXML()

if __name__ == "__main__":
    main()

