import numpy as np
import sys, os
import yaml

bkgFun = {}
SpuSys = {}

def readBkgPar():
   
    bkglist = [] 
    fin = open("bkgPara.txt", "r")
    for fline in fin.readlines():
        fline = fline.rstrip("\n")
        bkglist.append(fline)

    index = 0
    for i in range(len(bkglist)):
        if "Had" in bkglist[i] or "Lep" in bkglist[i]:
           index = index + 1
           print bkglist[i]
           cate = "cate{0}".format(str(index))
           bkgFun[cate] = bkglist[i+2]  
           SpuSys[cate] = bkglist[i+1]
    print bkgFun
    print SpuSys  
        #if "sigYield" in fline: sigYield[fline.split()[0].split("_")[3]] = fline.split()[1]

def writeBkgXML():

    readBkgPar()

    for i in range(15):
       cate = "cate{0}".format(i+1)
       filename = "background_ttHCP_{0}.xml".format(cate)
       f = open(filename, "w")
       f.write("<!DOCTYPE Model SYSTEM 'AnaWSBuilder.dtd'>\n")
       f.write("<Model Type=\"UserDef\">\n")
       if "Power" in bkgFun[cate]:
          print "Using PowerLaw1 Function"
          f.write("<ModelItem Name=\"EXPR::bkgPdf_ttHCP_{0}('pow(@0,@1)',:observable:,p1_:category:[-1,-100,0])\"/>\n".format(cate)) 
       elif "Exp" in bkgFun[cate]:
          print "Using Exp Function"
          f.write("<ModelItem Name=\"EXPR::bkgPdf_ttHCP_{0}('exp(@0*@1)',:observable:,p1_:category:[0,-1000,1000])\"/>\n".format(cate))
       else:
          print "ERROR"
       f.write("</Model>\n")

def writeSpuSys():

    readBkgPar()

    for i in range(15):
       cate = "cate{0}".format(i+1)
       filename = "ttHCP_{0}.xml".format(cate)
       cmd = 'sed -i "s/SPURIOUS/{0}/g" {1}'.format(SpuSys[cate], filename)
       os.system(cmd)

def main():
    #readBkgPar()
    #writeSigpara(i)
    #writeBkgXML()
    writeSpuSys()

if __name__ == "__main__":
    main()

