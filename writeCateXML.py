#!/usr/bin/python2.6

import sys, os
import shutil
import getpass
import glob,fileinput
from ROOT import TFile,TTree,TChain,TBranch,TH1,TH1F,TList,TH3,TH3F,TLine

from writeBkgPDF import writeSpuSys

def ttHCPxml():
    temp = "template.xml"
    rootfile = "total_sig.root"
    fin = TFile(rootfile)

    for i in range(15):

        histtag = "cate{0}".format(str(i+1))
        h_ttheven = fin.Get("ttH_a0_{0}_m_yy".format(histtag))
	h_tthodd = fin.Get("ttH_a90_{0}_m_yy".format(histtag))
        h_twheven = fin.Get("tWH_a0_{0}_m_yy".format(histtag))
        h_twhodd = fin.Get("tWH_a90_{0}_m_yy".format(histtag))
        h_thjbeven = fin.Get("tHjb_a0_{0}_m_yy".format(histtag))
        h_thjbodd = fin.Get("tHjb_a90_{0}_m_yy".format(histtag))
        h_bbh = fin.Get("bbH_{0}_m_yy".format(histtag))
        h_ggzh = fin.Get("ggZH_{0}_m_yy".format(histtag))
        h_ggh = fin.Get("ggH_a0_{0}_m_yy".format(histtag))
        h_wh = fin.Get("WH_{0}_m_yy".format(histtag))
        h_zh = fin.Get("ZH_{0}_m_yy".format(histtag))
        h_vbf = fin.Get("VBF_{0}_m_yy".format(histtag))
      
        evt_ttheven = h_ttheven.Integral()
        evt_tthodd = h_tthodd.Integral()
        evt_twheven = h_twheven.Integral()
        evt_twhodd = h_twhodd.Integral()
        evt_thjbeven = h_thjbeven.Integral()
        evt_thjbodd = h_thjbodd.Integral()
        evt_bbh = h_bbh.Integral()
        evt_ggzh = h_ggzh.Integral()
        evt_ggh = h_ggh.Integral()
        evt_wh = h_wh.Integral()
        evt_zh = h_zh.Integral()
        evt_vbf = h_vbf.Integral()
       
        filename = "ttHCP_cate{0}.xml".format(str(i+1))
        cmd1 = "cp {0} {1}".format(temp, filename)
        os.system(cmd1)
       
        cmd2 = 'sed -i "s/CATE/cate{0}/g" {1}'.format(str(i+1), filename)
        os.system(cmd2)
       
        #cmd3 = 'sed -i "s/HISTNAME/{0}/g" {1}'.format(histtag, filename)
        #os.system(cmd3)

        cmd4 = 'sed -i "s/ttHEVEN/{0}/g" {1}'.format(str(evt_ttheven), filename)
        os.system(cmd4)

        cmd5 = 'sed -i "s/ttHODD/{0}/g" {1}'.format(str(evt_tthodd), filename)
        os.system(cmd5)

        cmd6 = 'sed -i "s/BBH/{0}/g" {1}'.format(str(evt_bbh), filename)
        os.system(cmd6)

        cmd7 = 'sed -i "s/GGH/{0}/g" {1}'.format(str(evt_ggh), filename)
        os.system(cmd7)

        cmd8 = 'sed -i "s/VBF_YIELD/{0}/g" {1}'.format(str(evt_vbf), filename)
        os.system(cmd8)

        cmd9 = 'sed -i "s/WH_YIELD/{0}/g" {1}'.format(str(evt_wh), filename)
        os.system(cmd9)

        cmd10 = 'sed -i "s/ZH_YIELD/{0}/g" {1}'.format(str(evt_zh), filename)
        os.system(cmd10)

        cmd11 = 'sed -i "s/GGZH/{0}/g" {1}'.format(str(evt_ggzh), filename)
        os.system(cmd11)

        cmd12 = 'sed -i "s/tHjbEVEN/{0}/g" {1}'.format(str(evt_thjbeven), filename)
        os.system(cmd12)

        cmd13 = 'sed -i "s/tHjbODD/{0}/g" {1}'.format(str(evt_thjbodd), filename)
        os.system(cmd13)

        cmd14 = 'sed -i "s/tWHEVEN/{0}/g" {1}'.format(str(evt_twheven), filename)
        os.system(cmd14)

        cmd15 = 'sed -i "s/tWHODD/{0}/g" {1}'.format(str(evt_twhodd), filename)
        os.system(cmd15)

def printXML():

     for i in range(15):
         cate = "cate{0}".format(str(i+1))
         print "  <Input>projects/ttHyy_CP/config/v5/ttHCP_{0}.xml</Input>".format(cate)


def main():
    ttHCPxml()
    writeSpuSys()
    #printXML()   

if __name__ == "__main__":
    main()
   
